package gob.ine.cognos.listas.principal;
import java.awt.List;
import java.util.ArrayList;
import java.util.Scanner;

import gob.ine.cognos.listas.objetos.Autos;
import gob.ine.cognos.listas.objetos.Camionetas;
import gob.ine.cognos.listas.objetos.Motocicletas;

public class Main {
    public static void main(String[] args) {
       
        Scanner entrada = new Scanner(System.in);
       
        ArrayList<Autos> listaVehiculos = new ArrayList<>();
        ArrayList<Motocicletas> listaMotos = new ArrayList<>();
        ArrayList<Camionetas> listaCamionetas = new ArrayList<>();
       
        boolean salir = false;
        while (!salir) {   
       
            System.out.println(" INGRESE LA OPCION ");
            System.out.println(" 1 - crear vehiculo ");
            System.out.println(" 2 - Listar vehiculos ingresados");
            System.out.println(" 3 - Eliminar vehiculo ");
            System.out.println(" 4 - Modificar vehiculo ");
            System.out.println(" 0 - Salir ");
            
         
            int opcion = Integer.parseInt(entrada.nextLine());
           
         
            switch(opcion) {
                case 1: // "case" Ingresar nuevo vehiculo
                    
                    Autos a = new Autos();
                    Motocicletas b = new Motocicletas();
                    Camionetas c = new Camionetas();
                    System.out.println("Ingrese el codigo del vehiculo:");
                    a.setCodigo(entrada.nextLine());
                    System.out.println("Ingrese el tipo del vehiculo: \n 1. Autos \n 2. Camionetas \n 3. Motocicletas");
                    int categoria = Integer.parseInt(entrada.nextLine());
                    
                    System.out.println("Ingrese patente del vehiculo:");
             
                    a.setPatente(entrada.nextLine());
                    System.out.println("Ingrese marca del vehiculo: ");
             
                    a.setMarca(entrada.nextLine());
                    System.out.println("Ingrese Modelo (a�o): ");
             
                    a.setmodelo(Integer.parseInt(entrada.nextLine()));
                    System.out.println("Kilometraje");
             
                    a.setBencina(Integer.parseInt(entrada.nextLine()));

                    
        			switch (categoria) {
        			case 1:
        				System.out.println("Ingrese el numero de asientos: ");
                        a.setAsientos(entrada.nextLine());
        				break;
        			case 2:
        				System.out.println("Ingrese la capacidad de carga.");
        				c.setCapacidad(entrada.nextLine());
        				break;
        			case 3:
        				System.out.println("Ingrese El tipo de motocicleta (racing, custom, naked, etc.");
        				b.setTipo(entrada.nextLine());
        				break;
        			}
              
                    listaVehiculos.add(a);
                    listaMotos.add(b);
                    listaCamionetas.add(c);

                    
              
                    
                    System.out.println("VEHICULO CREADO: ");
                    System.out.println("Codigo " + a.getCodigo());
                    System.out.println("Patente " + a.getPatante());
                    System.out.println("Marca " + a.getMarca());
                    System.out.println("Modelo (a�o) " + a.getmodelo());
                    System.out.println("Kilometraje " + a.getBencina());
                    switch (categoria) {
        			case 1:
        				System.out.println("Numero de asientos " + a.getAsientos());
                        break;
        			case 2:
        				System.out.println("La capacidad es " + c.getCapacidad());
        				break;
        			case 3:
        				System.out.println("El tipo de motocicleta (racing, custom, naked, etc.)  " + b.getTipo());
        				break;
        			}
                    System.out.println();
                    
                    break;
                case 2: // Listar todos los vehiculos ingresados en la lista:
                   
                   for(int i = 0; i < listaVehiculos.size(); i++){ 
                       System.out.println("Auto: " + (i + 1));
                     
                       Autos auto =  listaVehiculos.get(i); 
                       Camionetas camion =  listaCamionetas.get(i);
                       Motocicletas moto =  listaMotos.get(i);
                       System.out.println("Codigo: " + auto.getCodigo());
                       System.out.println("patente: " + auto.getPatante());
                       System.out.println("Marca: " + auto.getMarca());
                       System.out.println("Modelo (a�o): " + auto.getmodelo());
                       System.out.println("Kilometraje: " + auto.getBencina());
                       if(auto.getAsientos()!=null)
                       System.out.println("Asientos: " + auto.getAsientos());
                       if(camion.getCapacidad()!=null)
                       System.out.println("La capacidad es " + camion.getCapacidad());
                       if(moto.getTipo()!=null)
                       System.out.println("El tipo de motocicleta (racing, custom, naked, etc.): " + moto.getTipo());
                   }
                   
                    break;
                case 3: // ELiminar vehiculo de la lista
                    System.out.println("Ingreser el codigo del vehiculo a eliminar: ");
                    String codigo = entrada.nextLine();
                    
                    for (int i = 0; i < listaVehiculos.size(); i++) {
                        Autos auto = listaVehiculos.get(i);
                      
                        if (codigo.equals(auto.getCodigo())) {
                      
                            listaVehiculos.remove(i);
                            listaCamionetas.remove(i);
                            listaMotos.remove(i);
                            System.out.println("Vehiculo eliminado");
                        }
                    }
                    
                    break;
                case 4: // Modificar vehiculo:
                    System.out.println(" Ingrese el codigo del vehiculo a editar");
                    codigo = entrada.nextLine();
             
                    for (int i = 0; i < listaVehiculos.size(); i++) {
                        Autos auto = listaVehiculos.get(i);
                        if (codigo.equals(auto.getCodigo())) {
                        	System.out.println("Patente: " + auto.getPatante());
                        	System.out.println("marca: " + auto.getMarca());
                            System.out.println("Modelo (a�o): " + auto.getmodelo());
                            System.out.println("Kilometraje: " + auto.getBencina());
                            System.out.println("Ingrese nueva Patente: ");
                            String patente = entrada.nextLine();
                            System.out.println("Ingrese nueva marca: ");
                            String marca = entrada.nextLine();
                            System.out.println("Ingrese nuevo modelo (a�o): ");
                            int modelo = entrada.nextInt();
                            String aux2 = entrada.nextLine();
                            System.out.println("Ingrese nuevo Kilometraje: ");
                            int kilometraje = (Integer.parseInt(entrada.nextLine()));
                            auto.setPatente(patente);
                            auto.setmodelo(modelo);
                            auto.setMarca(marca);
                            auto.setBencina(kilometraje);
                            
                        }
                    }
                    break;
                case 0:
                    salir = true;
                    System.out.println("Salir del programa");
                    break;
                default: 
                    System.out.println("Opcion invalida!!!!!!!");
                
            }
        }
        
    }
    
}