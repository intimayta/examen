package gob.ine.cognos.listas.objetos;

public class Autos {
	private String codigo;
	private String marca;
    private String patente;
    private int modelo;
    private int kilometraje;
    private String asientos;
    
    // 
    public Autos(){}
        
    
    public Autos(String codigo,String marca, String patente, int modelo, int kilometraje,String asientos)
    {
    	this.codigo = codigo;
    	this.marca = marca;
        this.patente = patente;
        this.modelo = modelo;
        this.kilometraje = kilometraje;
        this.asientos = asientos;
    }

    
    
    public String getAsientos() {
		return asientos;
	}

	public void setAsientos(String asientos) {
		this.asientos = asientos;
	}

	public String getCodigo() {
		return codigo;
	}

	public void setCodigo(String codigo) {
		this.codigo = codigo;
	}

	public String getMarca(){
        return this.marca;
    }
    
    public void setMarca(String marca) {
        this.marca = marca;
    }
    
    public void setPatente(String pat){
        this.patente = pat;
    }
    public String getPatante(){
        return this.patente;
    }
    public void setmodelo(int modelo){
        this.modelo = modelo;
    }
    public int getmodelo(){
        return this.modelo;
    }
    public void setBencina(int kilometraje){
        this.kilometraje = kilometraje;
    }
    public int getBencina(){
        return this.kilometraje;
    }   
}