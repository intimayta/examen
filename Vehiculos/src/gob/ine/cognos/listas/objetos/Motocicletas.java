package gob.ine.cognos.listas.objetos;

public class Motocicletas extends Autos {
	private String tipo;

	public Motocicletas(String codigo,String marca, String patente, int modelo, int kilometraje,String tipo) {
		super(codigo,marca,patente,modelo,kilometraje, tipo);
		this.tipo = tipo;
	}

	public Motocicletas() {

	}

	public String getTipo() {
		return tipo;
	}

	public void setTipo(String tipo) {
		this.tipo = tipo;
	}
	
}
