package gob.ine.cognos.listas.objetos;

public class Camionetas extends Autos {
	private String capacidad;

	public Camionetas(String codigo, String marca, int anio, int patente, int kilometraje, String capacidad) {
		super();
		this.capacidad = capacidad;
	}
	
	public Camionetas() {

	}

	public String getCapacidad() {
		return capacidad;
	}

	public void setCapacidad(String capacidad) {
		this.capacidad = capacidad;
	}
	
}
